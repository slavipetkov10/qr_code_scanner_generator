import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:qr_code_scanner_generator/page/qr_create_page.dart';
import 'package:qr_code_scanner_generator/page/qr_scan_page.dart';
import 'package:qr_code_scanner_generator/widget/ButtonWidget.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static final String title = 'QR Code Scanner';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: title,
      theme: ThemeData(
        primaryColor: Colors.red,
        scaffoldBackgroundColor: Colors.black,
      ),
      home: MainPage(title: title),
    );
  }
}

class MainPage extends StatefulWidget {
  final String title;

  const MainPage({
    @required this.title,
  });

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ButtonWidget(
              text: 'Create QR Code',
              onClicked: () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => QRCreatePage(),
                ),
              ),
            ),
            SizedBox(height: 32),
            ButtonWidget(
              text: 'Scan QR Code',
              onClicked: () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) =>
                      QRScanPage(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
